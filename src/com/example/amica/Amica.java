package com.example.amica;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class Amica extends Activity {

	TabHost th;
	TabSpec specs;
	TextView txtOpen;
	TextView txtMenu;
	TextView txtTitle; 
	Spinner day; 
	String dayOfWeek=""; 
	ProgressBar mProgress; 
	ProgressBar mProgress2; 	
	private int mProgressStatus = 0;
	private int mProgressStatus2 = 0;
	int date; 
	
	Handler mHandler = new Handler();
	static final String url = "http://www.amica.fi/en/tekuila";
	URI uri;
	HTMLReader reader = new HTMLReader();
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
		txtOpen = (TextView) findViewById(R.id.textView1);		
		day= (Spinner)findViewById(R.id.spinner1); 
		txtTitle = (TextView) findViewById(R.id.txtTitle);
		txtMenu = (TextView) findViewById(R.id.txtMenu);
		mProgress= (ProgressBar)findViewById(R.id.progressBar1);
		mProgress2= (ProgressBar)findViewById(R.id.progressBar2);
		Calendar calender= new GregorianCalendar(); 
		date=calender.get(Calendar.DAY_OF_WEEK);
		 try {
			 if (date ==1 || date==2||date==7)
			 {
				 dayOfWeek="Monday"; 
				 day.setSelection(getIndex(day, dayOfWeek));
			 }
			 else if(date == 3)
			 {
				 dayOfWeek="Tuesday"; 
				 day.setSelection(getIndex(day, dayOfWeek));
			 }
			 else if(date == 4)
			 {
				 dayOfWeek="Wednesday"; 
				 day.setSelection(getIndex(day, dayOfWeek));
			 }
			 else if(date == 5)
			 {
				 dayOfWeek="Thursday"; 
				 day.setSelection(getIndex(day, dayOfWeek));
			 }
			 else if(date == 6)
			 {
				 dayOfWeek="Friday"; 
				 day.setSelection(getIndex(day, dayOfWeek));
			 }
			uri = new URI("http://www.amica.fi/en/Ravintolat/Amica-restaurants/Areas/Rovaniemi/Rovaniemi-University-of-Applied-Sciences-the-Rantavitikka-Campus/");
			new ReadMenu().execute(uri);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		th = (TabHost) findViewById(R.id.tabhost);

		th.setup();

		specs = th.newTabSpec("tagMen");
		specs.setContent(R.id.tabMenu);
		specs.setIndicator("Menu");
		th.addTab(specs);

		specs = th.newTabSpec("tagOpen");
		specs.setContent(R.id.tabOpening);
		specs.setIndicator("Opening Hours");
		th.addTab(specs);

		th.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String arg0) {
				if (arg0.equals("tagOpen")) {
					Log.d("last result", " event open tab");
					mProgressStatus2=0;
					new ReadOpening().execute(uri);

				} else if (arg0.equals("tagMen")) {
					Log.d("last result", "event menu tab");
					mProgressStatus=0;
					new ReadMenu().execute(uri);
				}
			}
		});

		
		
		/**/
		day.setOnItemSelectedListener(
        		new OnItemSelectedListener() {
        		public void onNothingSelected(AdapterView<?> arg0) { }
        		public void onItemSelected(AdapterView<?> parent, View v,
        		int position, long id) {
        		 dayOfWeek= day.getSelectedItem().toString(); 
        		 txtMenu.setText(""); 
        		  mProgressStatus=0; 
        		 new ReadMenu().execute(uri);
        		}
        		});

	}
	private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		 
		return true;
	}

	class ReadOpening extends AsyncTask<URI, Void, String> {

		

		public String getOpeningHours(URI url, String tag1, String tag2) {
			String HTML = reader.getHtml(uri);
			int x = HTML.indexOf(tag1);
			x = HTML.indexOf("<p>", x);

			int y = HTML.indexOf(tag2, x);

			String content = HTML.substring(x + 3, y);
			Log.d("result index:", "" + x + "\t " + y);
			String result = content.replaceAll("&ndash;", "-");
			result = result.replaceAll("&nbsp;", "");
			result = result.replaceAll("stong", "");
			result = result.replaceAll("</h3>", "");
			result = result.replaceAll("<p>", "");
			result = result.replaceAll("</p>", "");
			result = result.replaceAll("<strong>", "");
			result = result.replaceAll("</strong>", "");
			result = result.replaceAll("<br />", "\n");

			
			return result;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			 while (mProgressStatus2 < 100) {
				txtOpen.setText(result);
                 mProgressStatus2 = 100;
                 mProgress2.setVisibility(View.GONE);
			 }
				Log.d("last result", "open tab");
		}

		@Override
		protected String doInBackground(URI... params) {
			String tag1 = "<h3 id=\"ctl00_RegionPageBody_RegionPage_RegionContent_RegionMainContent_"
					+ "RegionMainContentMiddle_RegionMainContentInnerMiddle_RegionMainContentText_"
					+ "MainContentBottomTextArea_MainContentToolBox_HeadingOpen";
			String tag2 = "</div";

			return getOpeningHours(params[0], tag1, tag2);
			}

	}

	class ReadMenu extends AsyncTask<URI, Void, String> {
		
				
		public String getMenu(URI url, String tag1, String tag2) {
			
			 String content= reader.getHtml(url);
			
			 int index1 = content.indexOf(tag1);
			 String result1 = content.substring(index1 + tag1.length());
			 
			 int index2 = result1.indexOf(tag1);
			 String result2 = result1.substring(index2 + tag1.length());
			 
			 result2 = result2.replace("<p>", "\n");
			 result2 = result2.replace("<br />", "\n");
			 result2 = result2.replace("&nbsp;", " ");
			 result2 = result2.replace("&ecirc;", "�");
			 result2 = result2.replace("&amp;", "&");
			 result2 = result2.replace("</p>", "");
			 
			 int index3 = result2.indexOf(tag2);
			 String finalResult = result2.substring(0, index3);
			 
			 return finalResult; 
		 }

		@Override
		protected String doInBackground(URI... params) {
			
			String tag1 = "<div class=\"ContentArea\">";
			String tag2 = "</div>";
			
			return getMenu(params[0], tag1, tag2);
		}
		
		public String getDayMenu(String result, String day)
		{			
			int x=0, y;
			String newResult= ""; 
			if(day.equals("week"))
			{
				y = result.indexOf("Monday");
				newResult= result.substring(x,y-2); 
				return newResult; 				
			}
			else if(day.equals("Monday"))
			{
				x = result.indexOf("Monday");
				y = result.indexOf("Tuesday"); 
				newResult= result.substring(x,y); 
				return newResult; 				
			}
			else if(day.equals("Tuesday"))
			{
				x = result.indexOf("Tuesday");
				y = result.indexOf("Wednesday"); 
				newResult= result.substring(x,y); 
				return newResult; 				
			}
			else if(day.equals("Wednesday"))
			{
				x = result.indexOf("Wednesday");
				y = result.indexOf("Thursday"); 
				newResult= result.substring(x,y); 
				return newResult; 				
			}
			else if(day.equals("Thursday"))
			{
				x = result.indexOf("Thursday");
				y = result.indexOf("Friday"); 
				newResult= result.substring(x,y); 
				return newResult; 				
			}
			else if(day.equals("Friday"))
			{
				x = result.indexOf("Friday");
				y = result.length(); 
				newResult= result.substring(x,y); 
				return newResult; 				
			}			
			return ""; 
		}
		
	
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
				
				try {
					String title= getDayMenu(result, "week"); 
					String dayMenu= getDayMenu(result,dayOfWeek);
					 while (mProgressStatus < 100) {
						 txtTitle.setText(title); 
						 txtMenu.setText(dayMenu);
	                     mProgressStatus = 100;
	                    // mProgress.setVisibility(View.GONE);
	                     mHandler.post(new Runnable() {
	                         public void run() {
	                             mProgress.setProgress(mProgressStatus);
	                         }
	                     });

					 }Log.d("last result", "menu tab" + date );
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}

	}
}

