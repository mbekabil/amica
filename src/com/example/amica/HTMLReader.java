package com.example.amica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class HTMLReader {
	
	private String html=""; 
	
	public String getHtml(URI url)
	{
		//URI url = new URI("http://www.amica.fi/en/Ravintolat/Amica-restaurants/Areas/Rovaniemi/Rovaniemi-University-of-Applied-Sciences-the-Rantavitikka-Campus/");
		try{
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);
		//HttpPost requestt = new HttpPost(url);
		HttpResponse response = client.execute(request);

		
		InputStream in = response.getEntity().getContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder str = new StringBuilder();
		String line = null;


		while((line = reader.readLine()) != null)
		{
		str.append(line);
		}
		in.close();
		html = str.toString();
			return html; 
		
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null; 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null; 
		}
		
	}
	

}
